<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mymodel extends CI_Model {

	public function ApprovePemberiTugas($id){
		$hasil=$this->db->query("UPDATE datakaryawan SET status='1' WHERE id='$id'");
    return $hasil;
	}

	public function KembalikanPemberiTugas($id){
		$hasil=$this->db->query("UPDATE datakaryawan SET status='3' WHERE id='$id'");
    return $hasil;
	}

	public function KembalikanPimpinan($id){
		$hasil=$this->db->query("UPDATE datakaryawan SET status='3' WHERE id='$id'");
    return $hasil;
	}

	public function RejectPemberiTugas($id){
		$hasil=$this->db->query("UPDATE datakaryawan SET status='4' WHERE id='$id'");
    return $hasil;
	}

	public function ApprovePimpinan($id){
		$hasil=$this->db->query("UPDATE datakaryawan SET status='2' WHERE id='$id'");
    return $hasil;
	}

	public function GetKaryawan(){
		$this->db->select("id,nama,dari_jam,sampai_jam,tgl,agenda_lembur,pemberi_tugas");
		$this->db->from("datakaryawan");
		$this->db->where("divisi",$this->session->userdata('divisi'));
		return $this->db->get();
	}

	public function GetUpdate($where="")
	{
		$data = $this->db->query('select * from datakaryawan '.$where);
    return $data->result_array();
	}

	public function GetMonitoring1($where="")
	{
		$data = $this->db->query('select * from monitoring '.$where);
    return $data->result_array();
	}

	public function GetInboxUpdate($where="")
	{
		$data = $this->db->query('select status from tb_inbox '.$where);
    return $data->result_array();
	}

	public function GetNamaAtasan(){
		$data = $this->db->query("select nama from biodata_karyawan where level = 'admin' AND divisi = '".$this->session->userdata('divisi')."'");
		return $data->result_array();
	}

	public function UpdateData($tableName,$data,$where){
    $res = $this->db->update($tableName,$data,$where);
    return $res;
  }

	public function GetKategori($where=""){
		$data = $this->db->query('select * from datakaryawan '.$where);
		return $data->result_array();
	}

	public function GetMonitoring(){
		$this->db->select("a.id, a.ket, a.awalp, a.akhirp, a.status");
		$this->db->from("monitoring a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama");
		$this->db->where("b.inisial",$this->session->userdata('inisial'));
		$this->db->order_by("awalp", "desc");
		return $this->db->get();
	}

	public function GetMonitoring2(){
		$this->db->select("a.id, a.ket, a.awalp, a.akhirp, a.status");
		$this->db->from("monitoring a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama");
		$this->db->where("b.divisi",$this->session->userdata('divisi'));
		$this->db->order_by("awalp", "desc");
		return $this->db->get();
	}

	public function getPemberiTugas(){
		$this->db->select("nama");
		$this->db->from("biodata_karyawan");
		$this->db->where("level","user");
		$this->db->where("inisial !=",$this->session->userdata('inisial'));
		$this->db->where("divisi",$this->session->userdata('divisi'));
		return $this->db->get();
 	}

	public function getStatusData($id){
		$this->db->select("status");
		$this->db->from("daakaryawan");
		$this->db->where("id",$id);
		return $this->db->get();
 	}

	function data($number,$offset){
		return $query = $this->db->get('datakaryawan',$number,$offset)->result();
	}

	function jumlah_data(){
		return $this->db->get('datakaryawan')->num_rows();
	}

	public function InsertData($tableName,$data){
    $res = $this->db->insert($tableName,$data);
    return $res;
  }

	public function myjoin(){
		$this->db->select("a.nama,a.dari_jam,a.sampai_jam,a.tgl,a.agenda_lembur,a.id,c.ket");
		$this->db->from("datakaryawan a");
		$this->db->join("biodata_karyawan b","b.nama = a.pemberi_tugas","left");
		$this->db->join("monitoring c","c.id = a.id","right");
		$this->db->where("b.inisial",$this->session->userdata('inisial'));
		$this->db->where("a.status","1");
		$this->db->where("c.status","1");
		return $this->db->get();
	}

	public function myjoinAdmin(){
		$this->db->select("a.nama,a.dari_jam,a.sampai_jam,a.tgl,a.agenda_lembur,a.id,b.ket");
		$this->db->from("datakaryawan a");
		$this->db->join("monitoring b","b.id = a.id","left");
		$this->db->where("a.status","4");
		$this->db->where("b.status","6");
		$this->db->where("divisi",$this->session->userdata('divisi'));
		return $this->db->get();
	}

	public function GetCetak(){
		$this->db->select("a.dari_jam,a.sampai_jam,a.tgl,a.agenda_lembur,a.pemberi_tugas,a.id");
		$this->db->from("datakaryawan a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama","left");
		$this->db->where("b.inisial",$this->session->userdata('inisial'));
		$this->db->where("a.status","7");
		return $this->db->get();
	}

	public function GetDataMonitoring(){
		$this->db->select("a.id,a.dari_jam,a.sampai_jam,a.tgl,a.agenda_lembur,a.pemberi_tugas");
		$this->db->from("datakaryawan a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama","left");
		$this->db->where("b.inisial",$this->session->userdata('inisial'));
		return $this->db->get();
	}

	public function GetDataMonitoring2(){
		$this->db->select("a.id,a.nama,a.dari_jam,a.sampai_jam,a.tgl,a.agenda_lembur,a.pemberi_tugas");
		$this->db->from("datakaryawan a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama","left");
		$this->db->where("b.divisi",$this->session->userdata('divisi'));
		return $this->db->get();
	}

	public function getInbox(){
		$this->db->select("a.waktu,a.id,c.agenda_lembur,c.pemberi_tugas,a.status");
		$this->db->from("tb_inbox a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama","left");
		$this->db->join("datakaryawan c","c.id = a.id","right");
		$this->db->where("b.inisial",$this->session->userdata('inisial'));
		$this->db->where("a.status <>","1");
		return $this->db->get();
	}

	public function getOutbox(){
		$this->db->select("a.waktu,a.id,c.agenda_lembur,c.pemberi_tugas,a.status");
		$this->db->from("tb_outbox a");
		$this->db->join("biodata_karyawan b","b.nama = a.nama","left");
		$this->db->join("datakaryawan c","c.id = a.id","right");
		$this->db->where("b.inisial",$this->session->userdata('inisial'));
		return $this->db->get();
	}

	public function buat_kode()   {
		  $this->db->select('RIGHT(datakaryawan.id,4) as kode', FALSE);
		  $this->db->order_by('id','DESC');
		  $this->db->limit(1);
		  $query = $this->db->get('datakaryawan');
		  if($query->num_rows() <> 0){
		   $data = $query->row();
		   $kode = intval($data->kode) + 1;
		  }
		  else {
		   $kode = 1;
		  }
		  return $kode;
	}

	public function GetNIK($where=""){
		$data = $this->db->query('select a.nik from biodata_karyawan a inner join datakaryawan b on a.nama = b.pemberi_tugas where b.id = '.$where);
		return $data->result_array();
	}

	public function GetAtasan($where=""){
		$div = $this->session->userdata('divisi');
		$data = $this->db->query("select a.nik,a.nama from biodata_karyawan a INNER JOIN datakaryawan b on a.divisi = b.divisi WHERE a.level='admin' and b.id= ".$where);
		return $data->result_array();
	}

}
