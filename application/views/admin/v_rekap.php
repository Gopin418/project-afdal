<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Approval</title>
    <link href="<?php echo base_url()."assets/css/"; ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()."assets/css/"; ?>bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />


  </head>
  <body>
    <section class="inputdata" id="inputdata">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3" style="padding-right:20px; border-right: 1px solid #ccc;">
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation"><a href="<?php echo base_url()."admin/index/"; ?>"><span class="glyphicon glyphicon-ok"></span> &nbsp; Approval</a></li>
              <li role="presentation"><a href="<?php echo base_url()."admin/monitoring/"; ?>"><span class="glyphicon glyphicon-eye-open"></span> &nbsp; Monitoring</a></li>
              <li role="presentation"><a href="<?php echo base_url()."admin/cetak/"; ?>"><span class="glyphicon glyphicon-file"></span> &nbsp; Cetak</a></li>
              <li role="presentation" class="active"><a href="#"><span class="glyphicon glyphicon-stats"></span> &nbsp; Rekap</a></li>
              <li role="presentation"><a href="<?php echo base_url()."admin/inbox/"; ?>"><span class="glyphicon glyphicon-inbox"></span> &nbsp; Inbox</a></li>
            </ul>
          </div>

          <!-- MENU APPROVAL -->

          <div class="col-sm-9">
            <h4>Rekap Data Karyawan</h4>
            <?php
                      $connect = mysqli_connect('localhost', 'root', '', 'projek_lembur');
                      $data_karyawan = mysqli_query($connect, "SELECT nama, jam, gaji FROM data_rekap ORDER BY jam DESC");

                      $data_nama = array();
                      $data_jam = array();
                      $data_gaji = array();

                      while ($data = mysqli_fetch_array($data_karyawan)) {

                        $data_nama[] = $data['nama']; // Memasukan nama ke dalam array
                        $data_jam[] = $data['jam']; // Memasukan jam ke dalam array
                        $data_gaji[] = $data['gaji']; //Memasukan gaji ke dalam array

                      }
                      ?>

                        <style>
                          .container {
                            width: 100%;
                            margin: 15px 10px;
                          }

                          .chart {
                            width: 50%;
                            float: left;
                            text-align: center;
                          }
                        </style>
                        <script src="<?php echo base_url().'assets/js/Chart.bundle.min.js';?>"></script>
                      </head>
                      <body>
                        <div class="container">
                          <div class="chart">
                      	  <select name="id_bulan">
                      	  <option></option>
                      	  <option>Januari</option>
                      	  <option>Februari</option>
                      	  <option>Maret</option>
                      	  <option>April</option>
                      	  <option>Mei</option>
                      	  <option>Juni</option>
                      	  <option>Juli</option>
                      	  <option>Agustus</option>
                      	  <option>September</option>
                      	  <option>Oktober</option>
                      	  <option>November</option>
                      	  <option>Desember</option>
                      	  </select>
                      	  <select name="id_bulan">
                      	  <option></option>
                      	  <option>2018</option>
                      	  <option>2019</option>
                      	  <option>2020</option>
                      	  <option>2021</option>
                      	  <option>2022</option>
                      	  <option>2023</option>
                      	  <option>2024</option>
                      	  <option>2025</option>
                      	  <option>2026</option>
                      	  <option>2027</option>
                      	  <option>2028</option>
                      	  <option>2029</option>
                          <br>

                      	  </select>
                            <canvas id="bar-chart"></canvas>
                          </div>



                        <script>
                          var barchart = document.getElementById('bar-chart');
                          var chart = new Chart(barchart, {
                            type: 'bar',
                            data: {

                              labels: <?php echo json_encode($data_nama) ?>, // Merubah data tanggal menjadi format JSON
                              datasets: [{
                                label: 'Data Lembur',
                                data: <?php echo json_encode($data_jam) ?>,

                                backgroundColor: [
                                  'rgba(0, 99, 132, 0.2)',
                                  'rgba(0, 99, 132, 0.2)',
                                  'rgba(0, 99, 132, 0.2)',
                                  'rgba(0, 99, 132, 0.2)',
                                  'rgba(0, 99, 132, 0.2)'
                                ],
                                borderColor: [
                                  'rgba(20,99,132,1)',
                                  'rgba(20,99,132,1)',
                                  'rgba(20,99,132,1)',
                                  'rgba(20,99,132,1)',
                                  'rgba(20,99,132,1)'
                                ],
                                borderWidth: 1
                              }]
                            }
                          });

                          var linechart = document.getElementById('line-chart');
                          var chart = new Chart(linechart, {
                            type: 'line',
                            data: {
                              labels: <?php echo json_encode($data_nama) ?>, // Merubah data tanggal menjadi format JSON
                              datasets: [{
                                label: 'Data Karyawan',
                                data: <?php echo json_encode($data_jam) ?>,
                                borderColor: 'rgba(255,99,132,1)',
                                backgroundColor: 'transparent',
                                borderWidth: 1
                              }]
                            }
                          });
                        </script>

          <!-- AKHIR MENU APPROVAL -->

          </div>
      </div>
    </section>



  <script src="<?php echo base_url().'assets/js/jquery-1.11.2.min.js';?>"></script>
  <script src="<?php echo base_url().'assets/js/bootstrap.js';?>"></script>
  <script src="<?php echo base_url().'assets/js/moment.js';?>"></script>

  </body>
</html>
