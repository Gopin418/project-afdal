<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
    parent::__construct();
    $this->load->model('mymodel');
		if($this->session->userdata('level')!="admin"){
			redirect('login');
		}
  }

	public function index(){
    $data['query'] = $this->mymodel->myjoinAdmin();
		$data['header'] = 'head';
		$data['content'] = 'admin/v_approval';
		$this->load->view('index',$data);
	}

	public function inbox(){
		$data['data_log']=$this->mymodel->getInbox()->result();
		$data['header'] = 'head';
		$data['content'] = 'admin/v_inbox';
		$this->load->view('index',$data);
	}

	public function prosesinbox(){
		$waktu = date("Y-m-d h:i:s");
		foreach ($_POST['pilih'] as $id) {
			$mhs = $this->mymodel->GetUpdate("where id = '$id'");
			$data = array(
				$status = $mhs[0]['status'],
				$nama = $mhs[0]['nama'],
				$pemberi_tugas = $mhs[0]['pemberi_tugas']
			);

			if($status == '6'){
				$waktu = date("Y-m-d h:i:s");

				$where2 = array(
					'id' => $id,
					'status' => '7'
				);

				$where3 = array(
					'id' => $id,
					'status' => '6'
				);

				$status3 = array('status' => '1');
				$where = array('id' => $id);
				$status = array('status' => '7');
				$status2 = array(
					'status' => '11',
					'akhirp' => $waktu
				);

				$data_insert = array(
					'id' => $id,
					'nama' => $nama,
					'waktu' => $waktu,
					'status' => '2'
				);
				$this->mymodel->UpdateData('datakaryawan',$status,$where);
				$this->mymodel->UpdateData('monitoring',$status2,$where2);
				$this->mymodel->UpdateData('tb_inbox',$status3,$where3);
				$this->mymodel->InsertData('tb_outbox',$data_insert);

				echo "<script>alert('Data berhasil diapprove');history.go(-1);";
				echo "window.location.href = '" . base_url() . "admin/index';</script>";
			}
		}
	}

	public function pilihapprove($id){
		$mhs = $this->mymodel->GetUpdate("where id = '$id'");
		$data = array(
			"id" => $mhs[0]['id'],
			"nama" => $mhs[0]['nama'],
			"nik" => $mhs[0]['nik'],
			"divisi" => $mhs[0]['divisi'],
			"lokasi" => $mhs[0]['lokasi'],
			"tgl" => $mhs[0]['tgl'],
			"dari_jam" => $mhs[0]['dari_jam'],
			"sampai_jam" => $mhs[0]['sampai_jam'],
			"agenda_lembur" => $mhs[0]['agenda_lembur'],
			"pemberi_tugas" => $mhs[0]['pemberi_tugas']
		);
		$data['header'] = 'head';
		$data['content'] = 'admin/approve';
		$this->load->view('index',$data);
	}

	public function prosesapproval(){
		$id = $_POST['id'];
		$nama = $_POST['nama'];
		$keterangan = $_POST['keterangan'];
		$pemberi_tugas = $_POST['penugas'];
		$waktu = date("Y-m-d h:i:s");

		$where2 = array(
			'id' => $id,
			'status' => '7'
		);

		$where3 = array(
			'id' => $id,
			'status' => '6'
		);

		$status3 = array('status' => '1');
		$where = array('id' => $id);

		if($this->input->post('btn') == "approve") {
			$status = array('status' => '7');
			$status2 = array(
				'status' => '11',
				'akhirp' => $waktu,
				'ket' => $keterangan
			);
			$data_insert = array(
				'id' => $id,
				'nama' => $nama,
				'ket' => $keterangan,
				'waktu' => $waktu,
				'status' => '2'
			);
			$this->mymodel->UpdateData('datakaryawan',$status,$where);
			$this->mymodel->UpdateData('monitoring',$status2,$where2);
			$this->mymodel->UpdateData('tb_inbox',$status3,$where3);
			$this->mymodel->InsertData('tb_outbox',$data_insert);

			echo "<script>alert('Data berhasil diapprove');history.go(-1);";
			echo "window.location.href = '" . base_url() . "admin/index';</script>";
		}

		elseif($this->input->post('btn') == "dikembalikan"){
			$status = array(
				'status' => '8',
				'akhirp' => $waktu,
				'ket' => $keterangan
			);

			$data_insert2=array(
				'id' => $id,
				'nama' => $pemberi_tugas,
				'waktu' => $waktu,
				'status' => '5'
			);

			$data_insert4=array(
				'id' => $id,
				'nama' => $nama,
				'awalp' => $waktu,
				'status' => '9'
			);
			$status2 = array('status' => '5');
			$this->mymodel->UpdateData('datakaryawan',$status2,$where);
			$this->mymodel->UpdateData('monitoring',$status,$where2);
			$this->mymodel->InsertData('monitoring',$data_insert4);
			$this->mymodel->InsertData('tb_inbox',$data_insert2);
			$this->mymodel->UpdateData('tb_inbox',$status3,$where3);

			echo "<script>alert('Data berhasil dikembalikan ke pemberi tugas');history.go(-1);";
			echo "window.location.href = '" . base_url() . "admin/index';</script>";
		}

		elseif ($this->input->post('btn') == "reject") {
			$data_insert3=array(
				'id' => $id,
				'nama' => $nama,
				'ket' => $keterangan,
				'waktu' => $waktu,
				'status' => '1'
			);

			$status = array(
				'status' => '12',
				'akhirp' => $waktu,
				'ket' => $keterangan
			);
			$status2 = array('status' => '5');
			$this->mymodel->UpdateData('datakaryawan',$status2,$where);
			$this->mymodel->UpdateData('monitoring2',$status,$where2);
			$this->mymodel->InsertData('tb_outbox',$data_insert3);
			$this->mymodel->UpdateData('tb_inbox',$status3,$where3);

			echo "<script>alert('Data berhasil direject');history.go(-1);";
			echo "window.location.href = '" . base_url() . "admin/index';</script>";
		}
	}

	public function edit_data($id){
		$mhs = $this->mymodel->GetUpdate("where id = '$id'");
		$mhd = $this->mymodel->GetMonitoring1("where id = '$id'");
		$mhn = $this->mymodel->GetInboxUpdate("where id = '$id'");
		$data = array(
			"id" => $mhs[0]['id'],
			"nama" => $mhs[0]['nama'],
			"nik" => $mhs[0]['nik'],
			"divisi" => $mhs[0]['divisi'],
			"lokasi" => $mhs[0]['lokasi'],
			"tgl" => $mhs[0]['tgl'],
			"dari_jam" => $mhs[0]['dari_jam'],
			"sampai_jam" => $mhs[0]['sampai_jam'],
			"agenda_lembur" => $mhs[0]['agenda_lembur'],
			"pemberi_tugas" => $mhs[0]['pemberi_tugas'],
			"keterangan" => $mhd[0]['ket'],
			$status = $mhs[0]['status']
		);

		if($status == '6'){
			$data['pemberi']=$this->mymodel->getPemberiTugas();
			$data['header'] = 'head';
			$data['content'] = 'admin/approve';
			$this->load->view('index',$data);
		}
	}

	public function cetak(){

		$data['data_log'] = $this->mymodel->GetKaryawan()->result();
		$data['data_log2'] = $this->mymodel->GetMonitoring()->result();
		$data['header'] = 'head';
		$data['content'] = 'admin/v_cetak';
		$this->load->view('index',$data);
	}

	public function rekap(){
		$data['header'] = 'head';
		$data['content'] = 'admin/v_rekap';
		$this->load->view('index',$data);
	}

	public function monitoring(){
			$data['data_log'] = $this->mymodel->GetDataMonitoring2()->result();
			$data['data_log2'] = $this->mymodel->GetMonitoring2()->result();
			$data['header'] = 'head';
			$data['content'] = 'admin/v_monitoring';
			$this->load->view('index',$data);
	}

}
